// 투명도 조절
    $(".slider-range").slider({
        orientation: "horizontal",
        range: "min",
        max: 100,
        slide: function(event, ui) {
            console.log('체험비 리턴 값:' + ui.value + '');
        }
    });


    function loadToggle(argument) {
        var _this = argument;
        if (_this.is('.active')) {
            _this.removeClass('active')
            $('.load-select').hide()
        } else {
            $('.select-window').hide()
            $('.btn-sort a,.layer-btn').removeClass('active')
            _this.addClass('active')
            $('.load-select').show()
        }
    }

    $(".load-select a,.satellite-select a").click(function() {
        var _this = $(this)
        if (_this.is('.active')) {
            _this.removeClass('active')
        } else {
            _this.addClass('active');
        }
        return false;
    })

    function legendToggle(argument) {
        var _this = argument;
        if (_this.is('.active')) {
            _this.removeClass('active')
            $('.legend-view').hide()
        } else {
            $('.select-window').hide()
            $('.btn-sort a,.layer-btn').removeClass('active')
            _this.addClass('active')
            $('.legend-view').show()
        }
    }

    function satelliteToggle(argument) {
        var _this = argument;
        if (_this.is('.active')) {
            _this.removeClass('active')
            $('.satellite-select').hide()
        } else {
            $('.select-window').hide()
            $('.btn-sort a,.layer-btn').removeClass('active')
            _this.addClass('active')
            $('.satellite-select').show()
        }
    }

    function layerToggle(argument) {
        var _this = argument;
        if (_this.is('.active')) {
            _this.removeClass('active')
            $('.layer-option').hide()
        } else {
            $('.select-window').hide()
            $('.btn-sort a,.layer-btn').removeClass('active')
            _this.addClass('active')
            $('.layer-option').show()
        }
    }

    // 검색창열기
    function searhList() {
        $('.search-list').show();
    }
    // 검색창닫기
    function searhListClose() {
        $('.search-list').hide();
    }

    function toggleList(arg) {
        var _this = arg;

        if (_this.is('.active')) {
            _this.removeClass('active')
                .parent('.toggle-header')
                .siblings('.toggle-content').stop().slideUp(300)
        } else {
            _this.addClass('active')
                .parent('.toggle-header')
                .siblings('.toggle-content').stop().slideDown(300)
        }
    }

    // gnb
    $('.gnb-li a').on('click', function() {
        var _target = $(this).parent().index();

        if ($(this).parent().index() == 0) {

        } else {
            if ($(this).is('.active')) {
                $(this).removeClass('active')
                $('.sub').removeClass('-open')
            } else {
                if ($('.sub').is('.-open')) {

                } else {
                    $('.sub').addClass('-open')
                }

                $('.sub-content-' + _target + '').addClass('active').siblings('.sub-content').removeClass('active');

                $(this).addClass('active').parent().siblings('li').find('a').removeClass('active')
            }

            return false;
        }
    })

    $('.btn-lnb-close').on('click', function() {
        $('.sub').removeClass('-open');
        $('.gnb-li a').removeClass('active')
    })

    // 탭메뉴 
    $('.tab-menu a.tab-toggle').on('click', function() {
        var _target = $($(this).attr('href'));

        if ($(this).is('.active')) {

        } else {
            _target.show().siblings('.tab-panel').hide()
            $(this).addClass('active').siblings('a').removeClass('active')
        }
    })